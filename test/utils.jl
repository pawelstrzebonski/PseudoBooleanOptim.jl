@testset "utils.jl" begin
    vals = 1:5
    res = 0

    # Function doesn't fail
    @test (res = PseudoBooleanOptim.pickrand(vals, vals); true)
    # Result is within the provided values
    @test res in vals
    @test (res = PseudoBooleanOptim.pickrand(vals, vals .* (vals .== 5)); true)
    # In this case, only 5 is possible result
    @test res == 5
end
