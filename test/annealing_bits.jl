@testset "annealing_bits.jl" begin
    # Function to optimize
    f(x) = sum(x)
    # Initial (worst) input
    x0 = BitArray(falses(5))
    res = 0

    # Function doesn't fail
    @test (res = PseudoBooleanOptim.annealing_bits(f, x0); true)
    # Result is surely better than worst result (0)
    @test f(res) > 0
end
