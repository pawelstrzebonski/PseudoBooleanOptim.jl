@testset "hill_climbing.jl" begin
    # Function to optimize
    f(x) = sum(x)
    # Initial (worst) input
    x0 = BitArray(falses(5))
    res = 0

    # Function doesn't fail
    @test (res = PseudoBooleanOptim.simple_hill_climbing(f, x0); true)
    # Result is surely better than worst result (0)
    @test f(res) > 0

    # Function doesn't fail
    @test (res = PseudoBooleanOptim.steepest_ascent_hill_climbing(f, x0); true)
    # Result is surely better than worst result (0)
    @test f(res) > 0

    # Function doesn't fail
    @test (res = PseudoBooleanOptim.stochastic_hill_climbing(f, x0); true)
    # Result is surely better than worst result (0)
    @test f(res) > 0

    # Function doesn't fail
    @test (res = PseudoBooleanOptim.random_restarting_hill_climbing(f, x0); true)
    # Result is surely better than worst result (0)
    @test f(res) > 0
end
