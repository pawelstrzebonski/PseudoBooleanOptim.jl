import PseudoBooleanOptim
import Test: @test_broken, @test, @test_throws, @testset

tests = [
    "utils",
    "random_walk",
    "hill_climbing",
    "simulated_annealing",
    "genetic",
    "optim",
    "annealing_bits",
]

approxeq(a, b; rtol = 1e-4) = all(isapprox.(a, b, rtol = rtol))

for t in tests
    @info "Running " * t * ".jl"
    include("$(t).jl")
    @info "Finished " * t * ".jl"
end
