@testset "optim.jl" begin
    # Function to optimize
    f(x) = sum(x)
    # Initial (worst) input
    x0 = BitArray(falses(5))
    res = 0

    # Function doesn't fail
    @test (
        res = PseudoBooleanOptim.optim(
            f,
            x0,
            method = PseudoBooleanOptim.random_walk,
            maxcalls = 10,
        );
        true
    )
    # Result is surely better than worst result (0)
    @test f(res) > 0

    # Function doesn't fail
    @test (
        res = PseudoBooleanOptim.optim(
            f,
            x0,
            method = PseudoBooleanOptim.random_restarting_hill_climbing,
            nrestarts = 10,
        );
        true
    )
    # Result is surely better than worst result (0)
    @test f(res) > 0

    # Function doesn't fail
    @test (
        res = PseudoBooleanOptim.optim(
            f,
            x0,
            method = PseudoBooleanOptim.simulated_annealing,
            t0 = 3,
            alpha = 2,
        );
        true
    )
    # Result is surely better than worst result (0)
    @test f(res) > 0
end
