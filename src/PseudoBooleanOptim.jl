module PseudoBooleanOptim

include("utils.jl")
include("random_walk.jl")
include("hill_climbing.jl")
include("simulated_annealing.jl")
include("genetic.jl")
include("annealing_bits.jl")
include("optim.jl")

export optim,
    random_walk,
    simple_hill_climbing,
    steepest_ascent_hill_climbing,
    stochastic_hill_climbing,
    random_restarting_hill_climbing,
    simulated_annealing,
    genetic_optimization,
    annealing_bits

end
