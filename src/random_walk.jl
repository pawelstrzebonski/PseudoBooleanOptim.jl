"""
    random_walk(
		f::Function,
		x0::BitArray;
		maxcalls::Integer = 100,
		maxtime::Number = Inf,
	)::BitArray

Random walk optimization, returning the best performing inputs (those
that maximize `f`) found during the random walk.

# Arguments
- `f`: function whose value is maximized.
- `x0`: starting input array for `f`.
- `maxcalls`: maximal number of function calls.
- `maxtime`: maximal time to spend within optimization loop (in Seconds).
"""
function random_walk(
    f::Function,
    x0::BitArray;
    maxcalls::Integer = 100,
    maxtime::Number = Inf,
)::BitArray
    # Initial index under test
    idx = 1
    # Create similar bit array
    x_best = copy(x0)
    x_test = similar(x0)
    # Initial performance
    y_best = f(x_best)
    y_test = y_best
    # Set starting time
    time0 = time()
    # Set function call counter
    calls = 1
    while calls < maxcalls && time() < time0 + maxtime
        # Modify random bit under test
        idx = rand(1:length(x_test))
        x_test[idx] = !x_test[idx]
        # Test the new array's performance
        y_test = f(x_test)
        calls += 1
        # If it is better, update the best array and value
        if y_test > y_best
            x_best .= x_test
            y_best = y_test
        end
    end
    x_best
end
