#TODO: other crossover methods?

"""
    crossover(x1::BitArray, x2::BitArray, prob::Number)::BitArray

Uniform crossover recombination of a pair of inputs, with each bit having
probability `prob` of coming from `x1` rather than `x2`.
"""
function crossover(x1::BitArray, x2::BitArray, prob::Number)::BitArray
    @assert size(x1) == size(x2)
    @assert 0 <= prob <= 1
    # Randomly choose which parent to inherit from for each bit based on prob
    choice = rand(size(x1)...) .< prob
    # Based on previous choice, select corresponding bits
    [c ? b1 : b2 for (c, b1, b2) in zip(choice, x1, x2)]
end

function mutate(x::BitArray, Nmutations::Number)::BitArray
    # Calculated per-bit probability to get expected # of mutations to be Nmutations
    prob = Nmutations / length(x)
    # Randomly determine whether each bit is mutated or not
    choice = rand(size(x)...) .< prob
    # Based on previous choice, mutate corresponding bits
    [c ? !b : b for (c, b) in zip(choice, x)]
end

#TODO: expose configurations
#TODO: do something to avoid premature convergence
#TODO: mutation schedule?
function genetic_optimization(
    f::Function,
    x0::BitArray;
    maxcalls::Integer = 100,
    Npopulation::Integer = 10,
    maxtime::Number = Inf,
)::BitArray
    # Generate random initial population and add initial seed to it
    population = [BitArray(rand(size(x0)...) .> 0.5) for i = 2:Npopulation]
    push!(population, x0)
    # Score the initial population and sort in descending fitness
    score = f.(population)
    idx = sortperm(score, rev = true)
    # Set starting time
    time0 = time()
    # Set function call counter
    calls = 0
    while calls < maxcalls && time() < time0 + maxtime
        # Expand population using mutated specimen and cross-over specimen (with top specimen x others)
        newpop = [
            population
            mutate.(population, div(length(x0), 100))
            crossover.(Ref(population[1]), population[2:end], 0.5)
        ]
        # Score the new population (skip scoring previous candidates)
        newscore = [score; f.(newpop[(1+Npopulation):end])]
        # Update # of function calls
        calls += 2 * Npopulation - 1
        # Sort the scores and pick top Npopulation
        idx = sortperm(newscore, rev = true)[1:Npopulation]
        # Save those top candidates and their scores
        population .= newpop[idx]
        score .= newscore[idx]
    end
    # Return the best candidate
    population[1]
end
