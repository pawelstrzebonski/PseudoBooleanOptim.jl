#TODO: Random index simple hill climbing?

"""
    simple_hill_climbing(
		f::Function,
		x0::BitArray;
		maxcalls::Integer = 100,
		maxtime::Number = Inf,
	)::BitArray

Hill climbing optimization, returning the best performing inputs (those
that maximize `f`) found. Will iteratively test changing a single bit
(in sequence) and keep positive changes.

# Arguments
- `f`: function whose value is maximized.
- `x0`: starting input array for `f`.
- `maxcalls`: maximal number of function calls.
- `maxtime`: maximal time to spend within optimization loop (in Seconds).
"""
function simple_hill_climbing(
    f::Function,
    x0::BitArray;
    maxcalls::Integer = 100,
    maxtime::Number = Inf,
)::BitArray
    # Initial index under test
    idx = 1
    # Create similar bit array
    x_best = copy(x0)
    x_test = similar(x0)
    # Initial performance
    y_best = f(x_best)
    y_test = y_best
    # Changes since last improvement
    lastimprovement = 0
    # Set starting time
    time0 = time()
    # Set function call counter
    calls = 1
    while calls < maxcalls && time() < time0 + maxtime
        # Copy the current best array and modify the bit under test
        x_test .= x_best
        x_test[idx] = !x_test[idx]
        # Test the new array's performance
        y_test = f(x_test)
        calls += 1
        # If it is better, update the best array and value
        if y_test > y_best
            x_best .= x_test
            y_best = y_test
            lastimprovement = 0
        else
            lastimprovement += 1
        end
        # If we haven't improved since going over entire array, call it quits
        if lastimprovement >= length(x_best)
            @info "Converged"
            break
        end
        # Move to next bit, if overflowing start from beginning
        idx += 1
        idx = idx <= length(x_best) ? idx : 1
    end
    x_best
end

"""
    steepest_ascent_hill_climbing(
		f::Function,
		x0::BitArray;
		maxcalls::Integer = 100,
		maxtime::Number = Inf,
	)::BitArray

Steepest-ascent hill climbing optimization, returning the best performing inputs (those
that maximize `f`) found. Will iteratively test all single bit changes
and accepting the best single bit change.

# Arguments
- `f`: function whose value is maximized.
- `x0`: starting input array for `f`.
- `maxcalls`: maximal number of function calls.
- `maxtime`: maximal time to spend within optimization loop (in Seconds).
"""
function steepest_ascent_hill_climbing(
    f::Function,
    x0::BitArray;
    maxcalls::Integer = 100,
    maxtime::Number = Inf,
)::BitArray
    # Initial index under test
    idx = 1
    idx_best = 1
    # Create similar bit array
    x_best = copy(x0)
    x_test = similar(x0)
    # Initial performance
    y_best = f(x_best)
    y_test = y_best
    y_tests = fill(y_best, length(x_test))
    # Set starting time
    time0 = time()
    # Set function call counter
    calls = 1
    while calls < maxcalls && time() < time0 + maxtime
        y_tests .= [
            begin
                # Copy the current best array and modify the bit under test
                x_test .= x_best
                x_test[idx] = !x_test[idx]
                # Test the new array's performance
                f(x_test)
            end for idx = 1:length(x_test)
        ]
        calls += length(x_test)
        # Find the best performing change
        y_test, idx_best = findmax(y_tests)
        # If it is better, update the best array and value
        if y_test > y_best
            x_best[idx_best] = !x_best[idx_best]
            y_best = y_test
        else
            # If no change improves, the call it quits
            @info "Converged"
            break
        end
    end
    x_best
end

"""
    stochastic_hill_climbing(
		f::Function,
		x0::BitArray;
		maxcalls::Integer = 100,
		maxtime::Number = Inf,
	)::BitArray

Stochastic hill climbing optimization, returning the best performing inputs (those
that maximize `f`) found. Will iteratively test all single bit changes
and will randomly accept one in proportion to the degree of improvement.

# Arguments
- `f`: function whose value is maximized.
- `x0`: starting input array for `f`.
- `maxcalls`: maximal number of function calls.
- `maxtime`: maximal time to spend within optimization loop (in Seconds).
"""
function stochastic_hill_climbing(
    f::Function,
    x0::BitArray;
    maxcalls::Integer = 100,
    maxtime::Number = Inf,
)::BitArray
    # Create similar bit array
    x_best = copy(x0)
    x_test = similar(x0)
    # Initial performance
    y_best = f(x_best)
    y_test = y_best
    y_tests = fill(y_best, length(x_test))
    # Set starting time
    time0 = time()
    # Set function call counter
    calls = 1
    while calls < maxcalls && time() < time0 + maxtime
        y_tests = [
            begin
                # Copy the current best array and modify the bit under test
                x_test .= x_best
                x_test[idx] = !x_test[idx]
                # Test the new array's performance
                f(x_test)
            end for idx = 1:length(x_test)
        ]
        calls += length(x_test)
        # Find all improvements
        uphillidx = findall(y_tests .> y_best)
        # If not possible improvement, call it quits
        if isempty(uphillidx)
            @info "Converged"
            break
        end
        # Randomly choose an improvement proportionally to the degree of improvement
        idx = pickrand(uphillidx, y_tests[uphillidx] .- y_best)
        # Update the array and value accordingly
        x_best[idx] = !x_best[idx]
        y_best = y_tests[idx]
    end
    x_best
end

"""
    random_restarting_hill_climbing(
		f::Function,
		x0::BitArray;
		maxcalls::Integer = 100,
		maxtime::Number = Inf,
		nrestarts::Integer = 100,
    )::BitArray

Randomly restarting hill climbing optimization, returning the best performing inputs (those
that maximize `f`) found. Will run multiple simple hill climbing calls
using random initial inputs, and returns the best result.

# Arguments
- `f`: function whose value is maximized.
- `x0`: starting input array for `f`.
- `maxcalls`: maximal number of function calls.
- `maxtime`: maximal time to spend within optimization loop (in Seconds).
- `nrestarts`: number of independent hill climbing optimizations.
"""
function random_restarting_hill_climbing(
    f::Function,
    x0::BitArray;
    maxcalls::Integer = 100,
    maxtime::Number = Inf,
    nrestarts::Integer = 100,
)::BitArray
    s = size(x0)
    # Generate a set of random points and add initial seed to it
    x0s = [BitArray(rand(s...) .> 0.5) for i = 2:nrestarts]
    push!(x0s, x0)
    # Optimize using simple hill climbing starting from each random point
    x_tests = [
        simple_hill_climbing(
            f,
            x0,
            maxtime = maxtime / nrestarts,
            maxcalls = div(maxcalls, nrestarts),
        ) for x0 in x0s
    ]
    # Calculate the value for each optimization result
    y_tests = f.(x_tests)
    # Return the best result
    x_tests[argmax(y_tests)]
end
