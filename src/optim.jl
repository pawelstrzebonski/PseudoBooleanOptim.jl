"""
    optim(
		f::Function,
		x0::BitArray;
		method::Function = random_restarting_hill_climbing,
		maxcalls::Integer = 100,
		maxtime::Number = Inf,
		args...,
	)::BitArray

Optimize binary array input of dimensions `x0` for the function `f` using
optimization algorithm implemented by the function `method` with a maximal
number of function calls `maxcalls` and maximal iteration time
`maxtime` (in Seconds). Additional options may be passed to the
optimizing algorithm (see documentation for optimizer specific functions).
"""
function optim(
    f::Function,
    x0::BitArray;
    method::Function = random_restarting_hill_climbing,
    maxcalls::Integer = 100,
    maxtime::Number = Inf,
    args...,
)::BitArray
    method(f, x0, maxcalls = maxcalls, maxtime = maxtime; args...)
end
