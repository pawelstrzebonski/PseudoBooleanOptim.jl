# Source: https://en.wikipedia.org/wiki/Simulated_annealing

"""
    probability(x_best::Number, x_test::Number, T::Number)

Return a probability of accepting a change based on the degree of improvement
(energy `x_test-x_best`) and temperature `T`.
"""
probability(x_best::Number, x_test::Number, T::Number) =
    x_test > x_best ? one(x_best) : exp(-(x_best - x_test) / T)

"""
    simulated_annealing(
		f::Function,
		x0::BitArray;
		maxcalls::Integer = 100,
		maxtime::Number = Inf,
		tempfun::Symbol = :linear,
		t0::Number = 1.0,
		alpha::Number = 1e-2,
	)::BitArray

Simulated annealing optimization, returning the best performing inputs (those
that maximize `f`) found.

# Arguments
- `f`: function whose value is maximized.
- `x0`: starting input array for `f`.
- `maxcalls`: maximal number of function calls.
- `maxtime`: maximal time to spend within optimization loop (in Seconds).
- `tempfun`: type of cooling schedule (`:linear`, `:geometric`, or `:slowdecrease`).
- `t0`: initial temperature.
- `alpha`: coefficient for the cooling schedule.
"""
function simulated_annealing(
    f::Function,
    x0::BitArray;
    maxcalls::Integer = 100,
    maxtime::Number = Inf,
    tempfun::Symbol = :linear,
    t0::Number = 1.0,
    alpha::Number = 1e-2,
)::BitArray
    # Define the cooling schedule
    function tf(t)
        if tempfun == :linear
            clamp(t - alpha, 0.0, Inf)
        elseif tempfun == :geometric
            clamp(t * alpha, 0.0, Inf)
        elseif tempfun == :slowdecrease
            clamp(t / (1 + alpha * t), 0.0, Inf)
        else
            error("Bad `tempfun`")
        end
    end
    # Pre-allocation and initial values
    x_best = copy(x0)
    x_test = copy(x_best)
    y_best = f(x_best)
    y_test = y_best
    T = t0
    idx = 1
    # Set starting time
    time0 = time()
    # Set function call counter
    calls = 1
    while calls < maxcalls && time() < time0 + maxtime
        # Update the temperature
        T = tf(T)
        # Randomly permute current vector
        x_test .= x_best
        idx = rand(1:length(x_test))
        x_test[idx] = !x_test[idx]
        # Calculate value of permuted vector
        y_test = f(x_test)
        calls += 1
        # Update randomly with a certain probability (based on degree of (non)improvement an temperature)
        if probability(y_best, y_test, T) > rand()
            x_best .= x_test
            y_best = y_test
        end
    end
    x_best
end
