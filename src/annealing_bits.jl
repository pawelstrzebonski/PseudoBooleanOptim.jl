"""
    annealing_bits(
		f::Function,
		x0::BitArray;
		maxiter::Integer = 100,
		alpha::Number = 2,
	)::BitArray

Hybrid hill climbing optimization algorithm with simulated annealing concepts,
returning the best performing inputs (those that maximize `f`) found.

In each iteration it randomly picks a bit with probability proportional
to it's "temperature", which it will flip and test if the flip increases
the figure of merit (FoM). If the bit-flip does, it's temperature will be increased
by a factor of `alpha`, else decreased by `alpha`.

The goal of this algorithm is that for complicated functions of many
binary inputs it will "learn" to ignore bits whose influence on the FoM
is straightforward (as these flipping these bits often will not
improve the FoM) and instead concentrate on bits whose influence on the
FoM is significant and complicated (ie highly dependent on other bits
and whose flipping will often improve the FoM).

# Arguments
- `f`: function whose value is maximized.
- `x0`: starting input array for `f`.
- `maxiter`: maximal number of iterations.
- `alpha`: factor by which to increase or decrease a bit's temperature.
"""
function annealing_bits(
    f::Function,
    x0::BitArray;
    maxiter::Integer = 100,
    alpha::Number = 2,
)::BitArray
    # Initial index under test
    idx = 1
    # Create similar bit array
    x_best = copy(x0)
    x_test = similar(x0)
    # Initial performance
    y_best = f(x_best)
    y_test = y_best
    # Bit temperature array
    T = ones(size(x0)...)
    for _ = 1:maxiter
        # Choose a random bit based on "temperature"
        idx = pickrand(1:length(x_test), vec(T))
        # Copy the current best array and modify the bit under test
        x_test .= x_best
        x_test[idx] = !x_test[idx]
        # Test the new array's performance
        y_test = f(x_test)
        # If it is better, update the best array and value
        if y_test > y_best
            x_best .= x_test
            y_best = y_test
            # Increase temperature if bit changed
            T[idx] *= alpha
        else
            # Decrease temperature if bit did not change
            #TODO: Should the increase/decrease be "asymmetric"?
            T[idx] /= alpha
        end
    end
    x_best
end
#TODO: tests and doc pages
