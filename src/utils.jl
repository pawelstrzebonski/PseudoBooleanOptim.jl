# Pick one of vals randomly with each having a probability proportional to probs
"""
    pickrand(vals::AbstractVector{T}, probs::AbstractVector)::T where {T}

Randomly pick an element of `vals` with probability proportional to `probs`.
"""
function pickrand(vals::AbstractVector{T}, probs::AbstractVector)::T where {T}
    @assert length(vals) == length(probs)
    # Convert probability weights into a CDF
    cdf = probs ./ sum(probs)
    cdf = [0.0; cumsum(cdf)]
    # Pick a random number
    x = rand()
    # Use CDF bins to determine which value to return
    vals[(cdf[1:end-1].<x).&(x.<=cdf[2:end])][1]
end
