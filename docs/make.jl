using Documenter
import PseudoBooleanOptim

makedocs(
    sitename = "PseudoBooleanOptim.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "PseudoBooleanOptim.jl"),
    pages = [
        "Home" => "index.md",
        "Examples" => "examples.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "annealing_bits.jl" => "annealing_bits.md",
            "genetic.jl" => "genetic.md",
            "hill_climbing.jl" => "hill_climbing.md",
            "optim.jl" => "optim.md",
            "random_walk.jl" => "random_walk.md",
            "simulated_annealing.jl" => "simulated_annealing.md",
            "utils.jl" => "utils.md",
        ],
        "test/" => [
            "annealing_bits.jl" => "annealing_bits_test.md",
            "genetic.jl" => "genetic_test.md",
            "hill_climbing.jl" => "hill_climbing_test.md",
            "optim.jl" => "optim_test.md",
            "random_walk.jl" => "random_walk_test.md",
            "simulated_annealing.jl" => "simulated_annealing_test.md",
            "utils.jl" => "utils_test.md",
        ],
    ],
)
