# utils.jl

## Description

Simple utility functions.

## Functions

```@autodocs
Modules = [PseudoBooleanOptim]
Pages   = ["utils.jl"]
```
