# Examples

The `PseudoBooleanOptim` package implements various optimization algorithms for
pseudo-Boolean functions. These functions take binary/Boolean inputs
and produce a real output. First let's define a very simple example
of such a function:

```@example optim
f(x) = sum(x)
```

Now, `PseudoBooleanOptim` will require working with the `BitArray` type
as inputs to these functions, so let's create an example input of this
type and evaluate the function we'll try to optimize:

```@example optim
# Create a BitArray of 5 `0` values
x0 = falses(5)
# And evaluate our function for this input
x0, f(x0)
```

Now, we chose a 5-element input `x0` that minimizes our function `f`. The
`PseudoBooleanOptim` algorithms will try to find an `x` that will maximize
`f(x)`. Let's use the `optim` function to try to do just that by providing
the function to be maximized and an example input (used as a starting
value for some algorithms):

```@example optim
# Import the package
import PseudoBooleanOptim

# And call the optimization routine
x_opt = PseudoBooleanOptim.optim(f, x0)
x_opt, f(x_opt)
```

Hopefully this will converge to the maximum solution of all 1's. Now,
what if we wanted to minimize `f(x)`? In that case we'd want to define
a new function that is maximized when `f(x)` is minimized, which we'll
call `g(x)`:

```@example optim
# This function will be maximized when f(x) is minimized
g(x) = -f(x)

# Let's start from a different point this time
x0 = trues(5)

# And now we maximize g(x)
x_opt = PseudoBooleanOptim.optim(g, x0)
x_opt, f(x_opt)
```

Hopefully this will converge to the maximum solution of all 0's. The
`optim` function will run a default optimization algorithm with some
default parameters which may not be appropriate to our problem. Especially
critical is the `maxcalls` argument that dictates the maximal number
of function calls the optimization algorithm may take when trying to maximize
our function. If the function is difficult to optimize, or if the input
has many bit elements, then we may need to define a (potentially much)
larger value of `maxcalls`. We can also change the algorithm being
used via the `method` option and define other algorithm-specific options:

```@example optim
# Our problem now takes on much larger inputs
x0 = falses(300)

x_opt = PseudoBooleanOptim.optim(
	f,
	x0,
	method = PseudoBooleanOptim.simple_hill_climbing,
	maxcalls = 700
)
x_opt, f(x_opt)
```
