# PseudoBooleanOptim.jl Documentation

## About

`PseudoBooleanOptim` provides some basic black-box optimization routines
for pseudo-Boolean functions (functions of Boolean/binary inputs and
real output).

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/PseudoBooleanOptim.jl
```

## Features

* Random walk algorithm
* Various hill climbing algorithms
* Basic simulated annealing
* Basic genetic algorithm optimization
