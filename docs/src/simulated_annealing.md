# simulated_annealing.jl

## Description

Implementation of simulated annealing algorithm.

## Functions

```@autodocs
Modules = [PseudoBooleanOptim]
Pages   = ["simulated_annealing.jl"]
```
