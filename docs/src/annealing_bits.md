# annealing_bits.jl

## Description

An experimental hybrid algorithm combining hill-climbing with simulated
annealing concepts.

## Functions

```@autodocs
Modules = [PseudoBooleanOptim]
Pages   = ["annealing_bits.jl"]
```
