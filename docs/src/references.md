# References

Pseudo-Boolean functions, as the
[Wikipedia article](https://en.wikipedia.org/wiki/Pseudo-Boolean_function)
describes, are functions of Boolean inputs that produce a real
output. Optimization of these functions falls under the umbrella of
[integer programming (Wikipedia)](https://en.wikipedia.org/wiki/Integer_programming),
or "zero-one linear programming (or binary integer programming)" to be
more precise. This package tries to implement various heuristic
algorithms for optimizing pseudo-Boolean functions, including some of those
listed on the
[Wikipedia article](https://en.wikipedia.org/wiki/Integer_programming#Heuristic_methods).

Hill climbing functions were primarily based on the algorithm
descriptions on the corresponding
[Wikipedia article](https://en.wikipedia.org/wiki/Hill_climbing).
Similarly, simulated annealing is also based on the
[Wikipedia article](https://en.wikipedia.org/wiki/Simulated_annealing).

Genetic algorithm related functions are, once again, based on descriptions
in the relevant Wikipedia articles for
[genetic algorithms](https://en.wikipedia.org/wiki/Genetic_algorithm),
[crossover](https://en.wikipedia.org/wiki/Crossover_\(genetic_algorithm\)),
[mutation](https://en.wikipedia.org/wiki/Mutation_(genetic_algorithm)),
and
[selection](https://en.wikipedia.org/wiki/Selection_\(genetic_algorithm\)).
