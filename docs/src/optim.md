# optim.jl

## Description

Wrapper function around the various optimization algorithms implemented
in this package.

## Functions

```@autodocs
Modules = [PseudoBooleanOptim]
Pages   = ["optim.jl"]
```
