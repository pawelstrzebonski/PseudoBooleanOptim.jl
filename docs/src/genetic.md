# genetic.jl

## Description

Implementation of a basic genetic algorithm based optimization.

## Functions

```@autodocs
Modules = [PseudoBooleanOptim]
Pages   = ["genetic.jl"]
```
