# random_walk.jl

## Description

Implementation of random walk algorithm.

## Functions

```@autodocs
Modules = [PseudoBooleanOptim]
Pages   = ["random_walk.jl"]
```
