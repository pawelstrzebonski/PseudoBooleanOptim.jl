# PseudoBooleanOptim

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://pawelstrzebonski.gitlab.io/PseudoBooleanOptim.jl)
[![Build Status](https://gitlab.com/pawelstrzebonski/PseudoBooleanOptim.jl/badges/master/pipeline.svg)](https://gitlab.com/pawelstrzebonski/PseudoBooleanOptim.jl/pipelines)
[![Coverage](https://gitlab.com/pawelstrzebonski/PseudoBooleanOptim.jl/badges/master/coverage.svg)](https://gitlab.com/pawelstrzebonski/PseudoBooleanOptim.jl/commits/master)

## About

`PseudoBooleanOptim.jl` is a Julia package for optimizing pseudo-Boolean
function (functions of Boolean/binary inputs and real output)..

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/PseudoBooleanOptim.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for PseudoBooleanOptim.jl](https://pawelstrzebonski.gitlab.io/PseudoBooleanOptim.jl/).
